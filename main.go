package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/golang/glog"

	"gitlab.com/grandsys/inu-qms-schedule/config"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
	"gitlab.com/grandsys/inu-qms-schedule/mongo"
	"gitlab.com/grandsys/inu-qms-schedule/router"
	"gitlab.com/grandsys/inu-qms-schedule/schedule"
)

func main() {
	configs := config.NewConfig()
	session := mongo.NewMongoSession(configs.MongoAddr)
	defer session.Close()

	mgoTask := mongo.NewMgoTask(session)
	elastic := elasticsearch.NewElastic(configs.ESAddr)
	uuid := controller.NewUUID()
	elasticController := controller.NewElasticController(elastic, uuid, configs.INUAddr)
	scheduler := schedule.NewScheduler(mgoTask, elasticController)
	scheduler.StartScheduler()

	router := router.NewRouter(configs, scheduler, elasticController)

	glog.Info("serve port", configs.Port)
	server := &http.Server{
		Addr:    configs.Port,
		Handler: router,
	}
	gracefulShutdown(server)
	if err := server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			glog.Info("Server closed under request")
		} else {
			glog.Fatal("Server closed unexpect")
		}
	}

	glog.Info("Server exiting")
}

func gracefulShutdown(server *http.Server) {
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		log.Println("receive interrupt signal")
		if err := server.Close(); err != nil {
			log.Fatal("Server Close:", err)
		}
	}()
}
