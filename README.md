# inu-qms-schedule


## How to run

```
$ ./inu-qms-schedule --help
Usage of inu-qms-schedule:
      --alsologtostderr                  log to standard error as well as files (default true)
      --datasource string                amiast datasource config (default "datasource.json")
      --es_addr string                   elasticserach address (default "http://localhost:9200")
      --inu_addr string                  inu address (default "http://localhost:2403")
      --log_backtrace_at traceLocation   when logging hits line file:N, emit a stack trace (default :0)
      --log_dir string                   If non-empty, write log files in this directory
      --logtostderr                      log to standard error instead of files
      --mongo_addr string                mongodb address (default "localhost:27017")
      --port string                      serve port (default "9487")
      --stderrthreshold severity         logs at or above this threshold go to stderr (default 2)
  -v, --v Level                          log level for V logs (default 2)
      --vmodule moduleSpec               comma-separated list of pattern=N settings for file-filtered logging
```

### Run with datasource

```
$ ./inu-qms-schedule --datasource=datasources.json
```

### Run with debug mode

```
$ ./inu-qms-schedule --datasource=datasources.json -v=4
```

### Run with release mode

```
$ export GIN_MODE=release
$ ./inu-qms-schedule --datasource=datasources.json
```

### Run tests

```
$ go test ./... -cover
```

### Update vendor directory using [govendor](https://github.com/kardianos/govendor)

```
$ govendor add +external
```

### Use [realize](https://github.com/tockins/realize) for development

You can open http://localhost:5001 in the browser to check Golang task

```
$ go get github.com/tockins/realize
$ realize start
```
