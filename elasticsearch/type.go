package elasticsearch

type ESInfo struct {
	ClusterName string  `json:"cluster_name"`
	ClusterUuid string  `json:"cluster_uuid"`
	Name        string  `json:"name"`
	Tagline     string  `json:"tagline"`
	Version     Version `json:"version,omitempty"`
}

type Version struct {
	BuildHash      string `json:"build_hash"`
	BuildSnapshot  bool   `json:"build_snapshot"`
	BuildTimestamp string `json:"build_timestamp"`
	LuceneVersion  string `json:"lucene_version"`
	Number         string `json:"number"`
}

type Result struct {
	ScrollID     string        `json:"_scroll_id,omitempty"`
	Took         int64         `json:"took,omitempty"`
	TimedOut     bool          `json:"timed_out,omitempty"`
	Shards       *Shards       `json:"_shards,omitempty"`
	Hits         *Hits         `json:"hits,omitempty"`
	Aggregations *Aggregations `json:"aggregations,omitempty"`
	ErrorMsg     *Error        `json:"error,omitempty"`
	Status       int64         `json:"status,omitempty"`
}

type Shards struct {
	Total      int64 `json:"total,omitempty"`
	Successful int64 `json:"successful,omitempty"`
	Failed     int64 `json:"failed,omitempty"`
}

type Hits struct {
	Total    int64   `json:"total,omitempty"`
	MaxScore float64 `json:"max_score,omitempty"`
	Hits     []*Hit  `json:"hits,omitempty"`
}

type Hit struct {
	Index  string  `json:"_index,omitempty"`
	Type   string  `json:"_type,omitempty"`
	ID     string  `json:"_id,omitempty"`
	Score  float64 `json:"_score,omitempty"`
	Source *Source `json:"_source,omitempty"`
}

type Source struct {
	Includes             []string `json:"includes,omitempty"`
	Excludes             []string `json:"excludes,omitempty"`
	Tags                 []string `json:"tags,omitempty"`
	Title                string   `json:"title,omitempty"`
	Query                *Query   `json:"query,omitempty"`
	StartTime            string   `json:"startTime,omitempty"`
	EndTime              string   `json:"endTime,omitempty"`
	Year                 int64    `json:"year,omitempty"`
	Quarter              int64    `json:"quarter,omitempty"`
	Month                int64    `json:"month,omitempty"`
	WeekNum              int64    `json:"weekNum,omitempty"`
	WeekDay              int64    `json:"weekDay,omitempty"`
	MonthDay             int64    `json:"monthDay,omitempty"`
	Length               int64    `json:"length,omitempty"`
	EndStatus            string   `json:"endStatus,omitempty"`
	ProjectName          string   `json:"projectName,omitempty"`
	AgentPhoneNo         string   `json:"agentPhoneNo,omitempty"`
	AgentID              string   `json:"agentId,omitempty"`
	AgentName            string   `json:"agentName,omitempty"`
	CallDirection        string   `json:"callDirection,omitempty"`
	CustomerGender       string   `json:"customerGender,omitempty"`
	CustomerPhoneNo      string   `json:"customerPhoneNo,omitempty"`
	MixLongestSilence    int64    `json:"mixLongestSilence,omitempty"`
	R0TotalInterruption  int64    `json:"r0TotalInterruption,omitempty"`
	R1TotalInterruption  int64    `json:"r1TotalInterruption,omitempty"`
	SumTotalInterruption int64    `json:"sumTotalInterruption,omitempty"`
	SilenceRatio         float64  `json:"silenceRatio,omitempty"`
	R0InterruptTimes     int64    `json:"r0InterruptTimes,omitempty"`
	R1InterruptTimes     int64    `json:"r1InterruptTimes,omitempty"`
	MixInterruptTimes    int64    `json:"mixInterruptTimes,omitempty"`
	TalkOverTimeRatio    float64  `json:"talkOverTimeRatio,omitempty"`
	R0TalkRatio          float64  `json:"r0TalkRatio,omitempty"`
	R1TalkRatio          float64  `json:"r1TalkRatio,omitempty"`
	R0SpeakSpeed         float64  `json:"r0SpeakSpeed,omitempty"`
	R1SpeakSpeed         float64  `json:"r1SpeakSpeed,omitempty"`
}

type Aggregations struct {
	AllTags AllTags `json:"all_tags,omitempty"`
}

type AllTags struct {
	DocCountErrorUpperBound int64    `json:"doc_count_error_upper_bound,omitempty"`
	SumOtherDocCount        int64    `json:"sum_other_doc_count,omitempty"`
	Buckets                 []Bucket `json:"buckets,omitempty"`
}

type Bucket struct {
	Key      string `json:"key,omitempty"`
	DocCount int64  `json:"doc_count,omitempty"`
}

type Querys struct {
	From   int64   `json:"from,omitempty"`
	Size   int64   `json:"size,omitempty"`
	Query  *Query  `json:"query,omitempty"`
	Source *Source `json:"_source,omitempty"`
}

type Query struct {
	Bool *Bool `json:"bool,omitempty"`
}

type Bool struct {
	MinimumShouldMatch int64          `json:"minimum_should_match,omitempty"`
	Must               []*ClauseQuery `json:"must,omitempty"`
	MustNot            []*ClauseQuery `json:"must_not,omitempty"`
	Should             []*ClauseQuery `json:"should,omitempty"`
}

type ClauseQuery struct {
	Bool       *Bool       `json:"bool,omitempty"`
	SpanNear   *SpanNear   `json:"span_near,omitempty"`
	MultiMatch *MultiMatch `json:"multi_match,omitempty"`
	Range      *Range      `json:"range,omitempty" bson:"range"`
	Term       *Term       `json:"term,omitempty" bson:"term"`
	Terms      *Terms      `json:"terms,omitempty" bson:"terms"`
}

type SpanNear struct {
	Clauses         []Clause `json:"clauses,omitempty,omitempty"`
	CollectPayloads bool     `json:"collect_payloads"`
	InOrder         bool     `json:"in_order"`
	Slop            int64    `json:"slop"`
}

type MultiMatch struct {
	Fields   string `json:"fields,omitempty"`
	Operator string `json:"operator,omitempty"`
	Query    string `json:"query,omitempty"`
}

type Clause struct {
	SpanTerm *SpanTerm `json:"span_term,omitempty"`
}

type SpanTerm struct {
	Dialogs   string `json:"dialogs,omitempty"`
	Agent0    string `json:"agent0,omitempty"`
	Customer0 string `json:"customer0,omitempty"`
}

type Term struct {
	Year            int64  `json:"year,omitempty" bson:"year"`
	Quarter         int64  `json:"quarter,omitempty" bson:"quarter"`
	Month           int64  `json:"month,omitempty" bson:"month"`
	WeekNum         int64  `json:"weekNum,omitempty" bson:"weekNum"`
	WeekDay         int64  `json:"weekDay,omitempty" bson:"weekDay"`
	MonthDay        int64  `json:"monthDay,omitempty" bson:"monthDay"`
	Length          int64  `json:"length,omitempty" bson:"length"`
	AgentID         string `json:"agentId,omitempty" bson:"agentId"`
	AgentName       string `json:"agentName,omitempty" bson:"agentName"`
	AgentPhoneNo    string `json:"agentPhoneNo,omitempty" bson:"agentPhoneNo"`
	CallDirection   string `json:"callDirection,omitempty" bson:"callDirection"`
	CustomerPhoneNo string `json:"customerPhoneNo,omitempty" bson:"customerPhoneNo"`
	CustomerGender  string `json:"customerGender,omitempty" bson:"customerGender"`
	ProjectName     string `json:"projectName,omitempty" bson:"projectName"`
	EndStatus       string `json:"endStatus,omitempty" bson:"endStatus"`
}

type Terms struct {
	Year            []int64  `json:"year,omitempty" bson:"year"`
	Quarter         []int64  `json:"quarter,omitempty" bson:"quarter"`
	Month           []int64  `json:"month,omitempty" bson:"month"`
	WeekNum         []int64  `json:"weekNum,omitempty" bson:"weekNum"`
	WeekDay         []int64  `json:"weekDay,omitempty" bson:"weekDay"`
	MonthDay        []int64  `json:"monthDay,omitempty" bson:"monthDay"`
	AgentID         []string `json:"agentId,omitempty" bson:"agentId"`
	AgentName       []string `json:"agentName,omitempty" bson:"agentName"`
	AgentPhoneNo    []string `json:"agentPhoneNo,omitempty" bson:"agentPhoneNo"`
	CustomerPhoneNo []string `json:"customerPhoneNo,omitempty" bson:"customerPhoneNo"`
}

type Range struct {
	StartTime            *DateCompare  `json:"startTime,omitempty" bson:"startTime"`
	EndTime              *DateCompare  `json:"endTime,omitempty" bson:"endTime"`
	Year                 *Int64Compare `json:"year,omitempty" bson:"year"`
	Quarter              *Int64Compare `json:"quarter,omitempty" bson:"quarter"`
	Month                *Int64Compare `json:"month,omitempty" bson:"month"`
	WeekNum              *Int64Compare `json:"weekNum,omitempty" bson:"weekNum"`
	WeekDay              *Int64Compare `json:"weekDay,omitempty" bson:"weekDay"`
	MonthDay             *Int64Compare `json:"monthDay,omitempty" bson:"monthDay"`
	Length               *Int64Compare `json:"length,omitempty" bson:"length"`
	R0TotalInterruption  *Int64Compare `json:"r0TotalInterruption,omitempty" bson:"r0TotalInterruption"`
	R1TotalInterruption  *Int64Compare `json:"r1TotalInterruption,omitempty" bson:"r1TotalInterruption"`
	SumTotalInterruption *Int64Compare `json:"sumTotalInterruption,omitempty" bson:"sumTotalInterruption"`
	MixLongestSilence    *Int64Compare `json:"mixLongestSilence,omitempty" bson:"mixLongestSilence"`
}

type Int64Compare struct {
	From int64 `json:"from,omitempty" bson:"from"`
	TO   int64 `json:"to,omitempty" bson:"to"`
	Gt   int64 `json:"gt,omitempty" bson:"gt"`
	Gte  int64 `json:"gte,omitempty" bson:"gte"`
	Lt   int64 `json:"lt,omitempty" bson:"lt"`
	Lte  int64 `json:"lte,omitempty" bson:"lte"`
}

type DateCompare struct {
	Gt     string `json:"gt,omitempty" bson:"gt"`
	Gte    string `json:"gte,omitempty" bson:"gte"`
	Lt     string `json:"lt,omitempty" bson:"lt"`
	Lte    string `json:"lte,omitempty" bson:"lte"`
	From   string `json:"from,omitempty" bson:"from"`
	TO     string `json:"to,omitempty" bson:"to"`
	Format string `json:"format,omitempty" bson:"format"`
}

type Error struct {
	RootCause    []*Reason      `json:"root_cause,omitempty"`
	Type         string         `json:"type,omitempty"`
	Reason       string         `json:"reason,omitempty"`
	Phase        string         `json:"phase,omitempty"`
	Grouped      bool           `json:"grouped,omitempty"`
	FailedShards []*FailedShard `json:"failed_shards,omitempty"`
}

type FailedShard struct {
	Shard  int64   `json:"shard,omitempty"`
	Index  string  `json:"index,omitempty"`
	Node   string  `json:"node,omitempty"`
	Reason *Reason `json:"reason,omitempty"`
}

type Reason struct {
	Type   string `json:"type,omitempty"`
	Reason string `json:"reason,omitempty"`
}
