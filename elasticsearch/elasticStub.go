package elasticsearch

import "strconv"
import "github.com/golang/glog"

func NewStubElastic() *elasticStub {
	return &elasticStub{}
}

type elasticStub struct {
}

func (s *elasticStub) Request(method, path string, body interface{}, v interface{}) error {
	return nil
}

func (s *elasticStub) GetTags() (*Result, error) {
	return &Result{
		Aggregations: &Aggregations{
			AllTags: AllTags{
				Buckets: []Bucket{
					{
						Key: "aaa",
					},
					{
						Key: "bbb",
					},
				},
			},
		},
	}, nil
}

func (s *elasticStub) GetModels(tags, from, size string) (*Result, error) {
	return &Result{
		Hits: &Hits{
			Hits: []*Hit{
				{
					ID: "1",
					Source: &Source{
						Title: "1",
						Tags:  []string{"a", "b"},
					},
				},
				{
					ID: "2",
					Source: &Source{
						Title: "2",
						Tags:  []string{"b", "c"},
					},
				},
			},
		},
	}, nil
}

func (e *elasticStub) GetPercolators(ids []string) (*Result, error) {
	return &Result{
		Hits: &Hits{
			Hits: []*Hit{
				{
					ID: "1",
					Source: &Source{
						Query: &Query{
							Bool: &Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
			},
		},
	}, nil
}

func (e *elasticStub) GetTaskResult(mustBool, shouldBool, mustNotBool []*ClauseQuery, size int64) (*Result, error) {
	hits := []*Hit{
		{
			Index: "index",
			Type:  "type",
			ID:    "id1",
			Source: &Source{
				Title: "1",
			},
		},
		{
			Index: "index",
			Type:  "type",
			ID:    "id2",
			Source: &Source{
				Title: "2",
			},
		},
		{
			Index: "index",
			Type:  "type",
			ID:    "id3",
			Source: &Source{
				Title: "3",
			},
		},
	}

	return &Result{
		ScrollID: "ScrollID",
		Hits: &Hits{
			Total: size,
			Hits:  hits[0:size],
		},
	}, nil
}

func (e *elasticStub) GetScroll(scrollID string) (*Result, error) {
	hits := []*Hit{
		{
			Index: "index",
			Type:  "type",
			ID:    "id1",
			Source: &Source{
				Title: "1",
			},
		},
		{
			Index: "index",
			Type:  "type",
			ID:    "id2",
			Source: &Source{
				Title: "2",
			},
		},
		{
			Index: "index",
			Type:  "type",
			ID:    "id3",
			Source: &Source{
				Title: "3",
			},
		},
	}
	scrollIDInt, err := strconv.Atoi(scrollID)
	if err != nil {
		glog.Fatal(err)
	}
	return &Result{
		Hits: &Hits{
			Total: int64(scrollIDInt),
			Hits:  hits[:scrollIDInt],
		},
	}, nil
}

func (e *elasticStub) DeleteScroll(scrollID string) error {
	return nil
}
