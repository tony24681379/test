package elasticsearch

import (
	"encoding/json"
	"strconv"
	"strings"

	"github.com/golang/glog"
)

type Elastic2 struct {
	*Elastic
}

func (e *Elastic2) GetTags() (*Result, error) {
	body := `
		{
			"size": 0,
			"aggs": {
				"all_tags": {
					"terms": {
						"field": "tags",
						"size": 100000000,
						"exclude":"\\@.*"
					}
				}
			}
		}
		`
	var result *Result
	if err := e.Request("GET", "/stored-query/.percolator/_search", body, &result); err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}

func (e *Elastic2) GetModels(tags, from, size string) (*Result, error) {
	must := `
	{
		"match": {
			"tags": {
				"query": "` + tags + `",
				"operator": "or"
			}
		}
	}
	`
	if tags == "" {
		must = ""
	}
	body := `
		{
			"size": ` + size + `,
			"from": ` + from + `,
			"query": {
			  	"bool": {
					"must": [
						` + must + `
					],
					"must_not": [
						{
							"match": {
								"tags": {
									"query": "@archived"
								}
							}
						},
						{
							"ids": {
								"type": "stored-query",
								"values": ["temporary"]
							}
						},
						{
						    "term": {
								 "temporary":true
							}
						}
					]
			  	}
			},
			"_source": ["title", "tags"]
		}		  
		`
	var result *Result
	if err := e.Request("GET", "/stored-query/.percolator/_search", body, &result); err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}

func (e *Elastic2) GetPercolators(ids []string) (*Result, error) {
	// {"1", "2"} -> 1","2
	id := strings.Join(ids, `","`)
	size := strconv.Itoa(len(ids))
	body := `
		{
			"size":` + size + `,
			"query": {
				"ids": {
					"values": ["` + id + `"]
				}
			},
			"_source": "query"
		}
		`
	var result *Result
	if err := e.Request("GET", "/stored-query/.percolator/_search", body, &result); err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}

func (e *Elastic2) GetTaskResult(mustBool, shouldBool, mustNotBool []*ClauseQuery, size int64) (*Result, error) {
	var querys *Querys
	querys = &Querys{
		Size: size,
		Query: &Query{
			Bool: &Bool{
				Must:    mustBool,
				Should:  shouldBool,
				MustNot: mustNotBool,
			},
		},
		Source: &Source{
			Excludes: []string{"customer*", "agent*", "dialogs", "vtt"},
		},
	}
	queryByte, err := json.Marshal(querys)
	if err != nil {
		return nil, err
	}
	var result *Result
	if err := e.Request("GET", "/logs-*/_search?scroll=1m", string(queryByte), &result); err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}
