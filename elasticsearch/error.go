package elasticsearch

import "fmt"

func (r *Result) Error() string {
	var err string
	for _, reason := range r.ErrorMsg.RootCause {
		err += reason.Reason
	}
	return fmt.Sprintf("%s", err)
}
