package elasticsearch

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/golang/glog"
	"github.com/parnurzeal/gorequest"
)

type ElasticInterface interface {
	Request(method, path string, body interface{}, v interface{}) error
	GetTags() (*Result, error)
	GetModels(tags, from, size string) (*Result, error)
	GetPercolators(ids []string) (*Result, error)
	GetTaskResult(mustBool, shouldBool, mustNotBool []*ClauseQuery, size int64) (*Result, error)
	GetScroll(scrollID string) (*Result, error)
	DeleteScroll(scrollID string) error
}

type Elastic struct {
	HTTPClient *gorequest.SuperAgent
	url        string // URL to Elasticsearch cluster
}

func NewElastic(url string) ElasticInterface {
	elastic := &Elastic{
		HTTPClient: gorequest.New(),
		url:        url,
	}
	version := elastic.getVersion()
	if version == '2' {
		return &Elastic2{
			Elastic: elastic,
		}
	} else if version == '5' {
		return &Elastic5{
			Elastic: elastic,
		}
	}
	glog.Fatal("doesn't match any version of Elasticsearch")
	return nil
}

// Request performs a request against `url` storing the results as `v` when non-nil.
func (e *Elastic) Request(method, path string, body interface{}, v interface{}) error {
	glog.V(4).Infof("method: %s, body: %s", method, body)

	res, b, errs := e.HTTPClient.CustomMethod(method, e.url+path).
		Set("Content-Type", "application/json").
		Retry(3, 1*time.Second, http.StatusBadRequest, http.StatusInternalServerError).
		Send(body).EndStruct(v)
	glog.V(4).Infof("StatusCode: %d, resp: %s", res.StatusCode, string(b))
	if errs != nil {
		var errstrings []string
		for _, e := range errs {
			errstrings = append(errstrings, e.Error())
		}
		return fmt.Errorf(strings.Join(errstrings, ","))
	}

	if res.StatusCode >= 300 {
		var result *Result
		err := json.Unmarshal(b, &result)
		if err != nil {
			return err
		}
		return result
	}
	return nil
}

func (e *Elastic) getVersion() byte {
	var esInfo *ESInfo
	if err := e.Request("GET", "/", "", &esInfo); err != nil {
		glog.Fatal(err)
	}
	version := esInfo.Version.Number[0]
	glog.Info("Elasticsearch version: " + string(version))
	return version
}

func (e *Elastic) GetScroll(scrollID string) (*Result, error) {
	body := `
			{
				"scroll" : "1m", 
				"scroll_id" : "` + scrollID + `"
			}
			`
	var result *Result
	if err := e.Request("GET", "/_search/scroll", body, &result); err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}

func (e *Elastic) DeleteScroll(scrollID string) error {
	body := `
			{
				"scroll_id" : ["` + scrollID + `"]
			}
			`
	if err := e.Request("DELETE", "/_search/scroll", body, nil); err != nil {
		glog.Error(err)
		return err
	}
	return nil
}
