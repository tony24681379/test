package elasticsearch

type Elastic5 struct {
	*Elastic
}

func (e *Elastic5) GetTags() (*Result, error) {
	return nil, nil
}

func (e *Elastic5) GetModels(tags, from, size string) (*Result, error) {
	return nil, nil
}

func (e *Elastic5) GetPercolators(ids []string) (*Result, error) {
	return nil, nil
}

func (e *Elastic5) GetTaskResult(mustBool, shouldBool, mustNotBool []*ClauseQuery, size int64) (*Result, error) {
	return nil, nil
}
