package schedule

import (
	"errors"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
	"gitlab.com/grandsys/inu-qms-schedule/mongo"
	"gitlab.com/grandsys/inu-qms-schedule/task"
)

func TestPreviewJob(t *testing.T) {
	tests := []struct {
		now           time.Time
		task          *task.Task
		size          int64
		expectedData  *controller.Data
		expectedError error
	}{
		{
			now: time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{
						All: []*elasticsearch.ClauseQuery{
							{
								Term: &elasticsearch.Term{
									Length: 2,
								},
							},
						},
					},
				},
			},
			size: 2,
			expectedData: &controller.Data{
				BatchInfo: &controller.BatchInfo{
					BatchCode:   "uuid",
					BatchStatus: 1,
					BatchSum:    2,
				},
				TaskRecord: []*controller.TaskRecord{
					{
						ID:        "id1",
						ReplayURL: "http://test/#!/index/type/id1",
						Source: &elasticsearch.Source{
							Title: "1",
						},
					},
					{
						ID:        "id2",
						ReplayURL: "http://test/#!/index/type/id2",
						Source: &elasticsearch.Source{
							Title: "2",
						},
					},
				},
				UUID: "uuid",
			},
			expectedError: nil,
		},
		{
			now: time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{},
				},
			},
			size:          2,
			expectedError: fmt.Errorf("Meta can't be all empty"),
		},
		{
			now: time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta:  &task.Meta{},
					Model: &task.Model{},
				},
			},
			size:          2,
			expectedError: fmt.Errorf("Meta can't be all empty, Model can't be all empty"),
		},
		{
			now: time.Date(2016, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{
						All: []*elasticsearch.ClauseQuery{
							{
								Term: &elasticsearch.Term{
									Length: 2,
								},
							},
						},
					},
				},
			},
			size:          2,
			expectedError: fmt.Errorf("Please make sure this job in a vaild execution time"),
		},
		{
			now: time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
			},
			size:          2,
			expectedError: fmt.Errorf("Expected 5 or 6 fields, found 4: * * * * "),
		},
	}
	assert := assert.New(t)
	stubElastic := elasticsearch.NewStubElastic()
	uuid := new(controller.UUIDStub)
	elastic := controller.NewElasticController(stubElastic, uuid, "http://test")
	fakeMongo := mongo.NewFakeTask(nil)
	scheduler := NewScheduler(fakeMongo, elastic)
	for i, test := range tests {

		data, err := scheduler.PreviewJob(test.now, test.task, test.size)
		assert.Equal(test.expectedData, data, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedError, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestAddJob(t *testing.T) {
	tests := []struct {
		now           time.Time
		tasks         []*task.Task
		task          *task.Task
		expectedTasks []*task.Task
		expectedErr   error
	}{
		{
			now:   time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			tasks: nil,
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					RuleCode:          "1",
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{},
				},
			},
			expectedTasks: []*task.Task{
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "1",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/12/31",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
			},
			expectedErr: nil,
		},
		{
			now: time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			tasks: []*task.Task{
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "1",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/12/31",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
			},
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					RuleCode:          "1",
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{},
				},
			},
			expectedTasks: []*task.Task{
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "1",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/12/31",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
			},
			expectedErr: nil,
		},
		{
			now: time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			tasks: []*task.Task{
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "1",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/12/31",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
			},
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					RuleCode:          "2",
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/06/30",
				},
				Condition: &task.Condition{
					Meta: &task.Meta{},
				},
			},
			expectedTasks: []*task.Task{
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "1",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/12/31",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
				{
					ScheduleRule: &task.ScheduleRule{
						RuleCode:          "2",
						Minute:            "*",
						Hour:              "*",
						MonthDay:          "*",
						Month:             "*",
						WeekDay:           "*",
						ScheduleStartAt:   "2017/01/01",
						ScheduleEndBefore: "2017/06/30",
					},
					Condition: &task.Condition{
						Meta: &task.Meta{},
					},
				},
			},
			expectedErr: nil,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		fakeMongo := mongo.NewFakeTask(test.tasks)
		scheduler := NewScheduler(fakeMongo, nil)
		err := scheduler.AddJob(test.now, test.task)
		assert.Equal(err, test.expectedErr, "Error in num: "+strconv.Itoa(i))
		result := scheduler.GetAllJobs()
		for _, expectedTask := range test.expectedTasks {
			assert.Contains(result, expectedTask, "Error in num: "+strconv.Itoa(i))
		}
	}
}

func TestGetJob(t *testing.T) {
	tasks := []*task.Task{
		{
			ScheduleRule: &task.ScheduleRule{
				RuleCode:          "1",
				Minute:            "*",
				Hour:              "*",
				MonthDay:          "*",
				Month:             "*",
				WeekDay:           "*",
				ScheduleStartAt:   "2017/01/01",
				ScheduleEndBefore: "2017/12/31",
			},
		},
		{
			ScheduleRule: &task.ScheduleRule{
				RuleCode:          "2",
				Minute:            "*",
				Hour:              "*",
				MonthDay:          "*",
				Month:             "*",
				WeekDay:           "*",
				ScheduleStartAt:   "2017/01/01",
				ScheduleEndBefore: "2017/06/30",
			},
		},
	}
	tests := []struct {
		ruleCode      string
		expectedTasks *task.Task
	}{
		{
			ruleCode: "1",
			expectedTasks: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					RuleCode:          "1",
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
			},
		},
		{
			ruleCode: "2",
			expectedTasks: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					RuleCode:          "2",
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/06/30",
				},
			},
		},
		{
			ruleCode:      "3",
			expectedTasks: nil,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		fakeMongo := mongo.NewFakeTask(tasks)
		scheduler := NewScheduler(fakeMongo, nil)
		result := scheduler.GetJob(test.ruleCode)
		assert.Equal(test.expectedTasks, result, "Error in num: "+strconv.Itoa(i))
	}
}

func TestRemoveJob(t *testing.T) {
	tasks := []*task.Task{
		{
			ScheduleRule: &task.ScheduleRule{
				RuleCode:          "1",
				Minute:            "*",
				Hour:              "*",
				MonthDay:          "*",
				Month:             "*",
				WeekDay:           "*",
				ScheduleStartAt:   "2017/01/01",
				ScheduleEndBefore: "2017/12/31",
			},
		},
		{
			ScheduleRule: &task.ScheduleRule{
				RuleCode:          "2",
				Minute:            "*",
				Hour:              "*",
				MonthDay:          "*",
				Month:             "*",
				WeekDay:           "*",
				ScheduleStartAt:   "2017/01/01",
				ScheduleEndBefore: "2017/06/30",
			},
		},
	}
	tests := []struct {
		ruleCode      string
		expectedError error
	}{
		{
			ruleCode:      "1",
			expectedError: nil,
		},
		{
			ruleCode:      "2",
			expectedError: nil,
		},
		{
			ruleCode:      "3",
			expectedError: errors.New("3 doesn't exist"),
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		stub := mongo.NewFakeTask(tasks)
		scheduler := NewScheduler(stub, nil)
		err := scheduler.RemoveJob(test.ruleCode)
		assert.Equal(test.expectedError, err, "Error in num: "+strconv.Itoa(i))
	}
}
