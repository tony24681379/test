package schedule

import (
	"time"

	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/job"
	"gitlab.com/grandsys/inu-qms-schedule/mongo"
	"gitlab.com/grandsys/inu-qms-schedule/task"
	cron "gopkg.in/robfig/cron.v2"
)

type Scheduler struct {
	cron              *cron.Cron
	mgoTask           mongo.TaskInterface
	elasticController controller.ControllerInterface

	// ruleCode -> cron.EntryID
	jobs map[string]cron.EntryID
	// ruleCode -> *job.Task
	taskJobs map[string]*task.Task
}

// NewScheduler create new Scheduler with mongo.TaskInterface and cron
func NewScheduler(mgoTask mongo.TaskInterface, elasticController controller.ControllerInterface) *Scheduler {
	scheduler := &Scheduler{
		cron:              cron.New(),
		mgoTask:           mgoTask,
		elasticController: elasticController,
		jobs:              make(map[string]cron.EntryID),
		taskJobs:          make(map[string]*task.Task),
	}

	if err := scheduler.reloadAllJob(); err != nil {
		glog.Fatal(err)
	}
	return scheduler
}

func (s *Scheduler) StartScheduler() {
	s.cron.Start()
}

func (s *Scheduler) PreviewJob(now time.Time, taskJob *task.Task, size int64) (*controller.Data, error) {
	if err := task.CheckVaildTask(now, taskJob); err != nil {
		return nil, err
	}
	data, scrollID, _, _, _, err := s.elasticController.GetFirstTask(taskJob, size)
	if scrollID != "" {
		s.elasticController.DeleteScroll(scrollID)
	}
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Scheduler) AddJob(now time.Time, taskJob *task.Task) error {
	if err := task.CheckVaildTask(now, taskJob); err != nil {
		return err
	}
	if err := s.mgoTask.AddOrEditTask(taskJob); err != nil {
		return err
	}

	if _, exist := s.jobs[taskJob.ScheduleRule.RuleCode]; exist == true {
		s.cron.Remove(s.jobs[taskJob.ScheduleRule.RuleCode])
	}

	cronRule := task.CombineCronRule(taskJob.ScheduleRule)
	entryID, err := s.cron.AddJob(cronRule, job.NewJob(taskJob, s.elasticController))
	if err != nil {
		return err
	}
	s.taskJobs[taskJob.ScheduleRule.RuleCode] = taskJob
	s.jobs[taskJob.ScheduleRule.RuleCode] = entryID
	glog.V(4).Info(s.cron.Entries())
	return nil
}

func (s *Scheduler) RemoveJob(ruleCode string) error {
	if err := s.mgoTask.DeleteTask(ruleCode); err != nil {
		return err
	}
	delete(s.taskJobs, ruleCode)
	delete(s.jobs, ruleCode)
	return nil
}

func (s *Scheduler) GetAllJobs() []*task.Task {
	tasks := []*task.Task{}
	for _, task := range s.taskJobs {
		tasks = append(tasks, task)
	}
	return tasks
}

func (s *Scheduler) GetJob(ruleCode string) *task.Task {
	return s.taskJobs[ruleCode]
}

func (s *Scheduler) reloadAllJob() error {
	taskJobs, err := s.mgoTask.GetAllTasks()
	if err != nil {
		return err
	}

	for _, taskJob := range taskJobs {
		cronRule := task.CombineCronRule(taskJob.ScheduleRule)
		entryID, err := s.cron.AddJob(cronRule, job.NewJob(taskJob, s.elasticController))
		if err != nil {
			return err
		}
		s.jobs[taskJob.ScheduleRule.RuleCode] = entryID
		s.taskJobs[taskJob.ScheduleRule.RuleCode] = taskJob
		glog.V(4).Info(s.cron.Entries())
	}
	return nil
}
