package job

import (
	"strconv"
	"testing"
	"time"

	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/controller/mocks"
	"gitlab.com/grandsys/inu-qms-schedule/task"

	"github.com/stretchr/testify/mock"
)

type requestStub struct {
}

func (r *requestStub) postData(body interface{}, path string) error {
	return nil
}

type TestControllerMock struct {
	mock.Mock
}

func (t *TestControllerMock) GetFirstTask(taskJob *task.Task, size int64) (data *controller.Data, scrollID, batchCode string, batchStatus, total int64, err error) {
	args := t.Called(taskJob, size)
	return args.Get(0).(*controller.Data), "scrollID", "batchCode", 0, 5, nil
}

func TestPostTask(t *testing.T) {
	mockController := new(mocks.ControllerMock)
	task := &task.Task{
		ScheduleRule: &task.ScheduleRule{
			BatchSize: 2,
			IsStart:   true,
		},
		Condition: nil,
	}
	mockController.On("GetFirstTask", task, int64(2)).Return(nil, "scrollID", "batchCode", int64(1), int64(1), nil)
	mockController.On("DeleteScroll", "scrollID")

	j := &Job{
		taskJob:           task,
		elasticController: mockController,
		request:           &requestStub{},
	}
	j.postTask(task)
	mockController.AssertExpectations(t)
}

func TestRun(t *testing.T) {
	mockController := new(mocks.ControllerMock)
	now := time.Now()
	startDate := strconv.Itoa(now.Year()) + "/" + strconv.Itoa(int(now.Month())) + "/" + strconv.Itoa(now.Day())
	endDate := strconv.Itoa(now.Year()+1) + "/" + strconv.Itoa(int(now.Month())) + "/" + strconv.Itoa(now.Day())
	task := &task.Task{
		ScheduleRule: &task.ScheduleRule{
			Minute:            "*",
			Hour:              "*",
			MonthDay:          "*",
			Month:             "*",
			WeekDay:           "*",
			BatchSize:         2,
			IsStart:           true,
			ScheduleStartAt:   startDate,
			ScheduleEndBefore: endDate,
		},
		Condition: &task.Condition{
			Meta: &task.Meta{},
		},
	}

	mockController.On("GetFirstTask", task, int64(2)).Return(nil, "scrollID", "batchCode", int64(0), int64(5), nil)
	mockController.On("GetScrollTask", task, "scrollID", "batchCode", mock.AnythingOfType("int64")).Return(nil, nil).Times(2)
	mockController.On("DeleteScroll", "scrollID")

	j := &Job{
		taskJob:           task,
		elasticController: mockController,
		request:           &requestStub{},
	}
	j.Run()
	mockController.AssertExpectations(t)
}
