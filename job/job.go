package job

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/task"

	"github.com/golang/glog"
	"github.com/parnurzeal/gorequest"
)

type Job struct {
	taskJob           *task.Task
	elasticController controller.ControllerInterface
	request           httpRequest
}

type httpRequest interface {
	postData(body interface{}, path string) error
}

type request struct {
	HTTPClient *gorequest.SuperAgent
}

func NewJob(taskJob *task.Task, elasticController controller.ControllerInterface) *Job {
	return &Job{
		taskJob:           taskJob,
		elasticController: elasticController,
		request:           &request{HTTPClient: gorequest.New()},
	}
}

func (j *Job) Run() {
	now := time.Now()
	if err := task.CheckVaildTask(now, j.taskJob); err != nil {
		return
	}
	if !j.taskJob.ScheduleRule.IsStart {
		return
	}
	glog.Info("job exec:", j.taskJob.ScheduleRule.RuleCode)
	j.postTask(j.taskJob)
}

func (j *Job) postTask(taskJob *task.Task) {
	batchSize := taskJob.ScheduleRule.BatchSize
	postPath := taskJob.ScheduleRule.CallBackURI
	data, scrollID, batchCode, batchStatus, total, err := j.elasticController.GetFirstTask(taskJob, batchSize)
	if err != nil {
		glog.Error(err)
	}
	defer func() {
		if scrollID != "" {
			j.elasticController.DeleteScroll(scrollID)
		}
	}()

	if err := j.request.postData(data, postPath); err != nil {
		glog.Error(err)
	}
	count := batchSize
	for batchStatus != 1 {
		glog.V(4).Info(count, batchSize)
		count += batchSize
		if count >= total {
			batchStatus = 1
		}
		data, err = j.elasticController.GetScrollTask(taskJob, scrollID, batchCode, batchStatus)
		if err := j.request.postData(data, postPath); err != nil {
			glog.Error(err)
		}
	}
}

func (r *request) postData(body interface{}, path string) error {
	glog.V(4).Infof("method: %s, body: %s", "POST", body)

	res, b, errs := r.HTTPClient.Post(path).
		Set("Content-Type", "application/json").
		Retry(3, 1*time.Second, http.StatusBadRequest, http.StatusInternalServerError).
		Send(body).End()
	glog.V(4).Infof("StatusCode: %d, resp: %s", res.StatusCode, string(b))
	if errs != nil {
		var errstrings []string
		for _, e := range errs {
			errstrings = append(errstrings, e.Error())
		}
		return fmt.Errorf(strings.Join(errstrings, ","))
	}

	return nil
}
