ARG GO_VERSION=1.9.2
FROM golang:${GO_VERSION}-alpine AS build-stage
WORKDIR /go/src/gitlab.com/grandsys/inu-qms-schedule
COPY ./ /go/src/gitlab.com/grandsys/inu-qms-schedule
RUN go test $(go list ./...)
RUN go install

FROM alpine:3.5
RUN apk add --no-cache tzdata curl
ENV TZ Asia/Taipei
COPY --from=build-stage /go/bin/inu-qms-schedule .
COPY ./datasources.json .
ENTRYPOINT ["./inu-qms-schedule"]