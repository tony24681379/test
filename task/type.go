package task

import "gitlab.com/grandsys/inu-qms-schedule/elasticsearch"

type Task struct {
	ScheduleRule *ScheduleRule `json:"scheduleRule,omitempty" bson:"scheduleRule"`
	Condition    *Condition    `json:"condition,omitempty" bson:"condition"`
}

type ScheduleRule struct {
	TaskType          string `json:"taskType,omitempty" bson:"taskType"`
	RuleCode          string `json:"ruleCode,omitempty" bson:"ruleCode"`
	Minute            string `json:"minute,omitempty" bson:"minute"`
	Hour              string `json:"hour,omitempty" bson:"hour"`
	MonthDay          string `json:"monthDay,omitempty" bson:"monthDay"`
	Month             string `json:"month,omitempty" bson:"month"`
	WeekDay           string `json:"weekDay,omitempty" bson:"weekDay"`
	BatchSize         int64  `json:"batchSize,omitempty" bson:"batchSize"`
	ScheduleStartAt   string `json:"scheduleStartAt,omitempty" bson:"scheduleStartAt"`
	ScheduleEndBefore string `json:"scheduleEndBefore,omitempty" bson:"scheduleEndBefore"`
	IsStart           bool   `json:"isStart,omitempty" bson:"isStart"`
	CallBackURI       string `json:"callBackURI,omitempty" bson:"callBackURI"`
}

type Condition struct {
	Model *Model `json:"model,omitempty" bson:"model"`
	Meta  *Meta  `json:"meta,omitempty" bson:"meta"`
}

type Model struct {
	All    []*ModelItem `json:"all,omitempty" bson:"all"`
	Any    []*ModelItem `json:"any,omitempty" bson:"any"`
	NotAll []*ModelItem `json:"not_all,omitempty" bson:"not_all"`
	NotAny []*ModelItem `json:"not_any,omitempty" bson:"not_any"`
}

type ClauseQuery = elasticsearch.ClauseQuery

type Meta struct {
	All    []*ClauseQuery `json:"all,omitempty" bson:"all"`
	Any    []*ClauseQuery `json:"any,omitempty" bson:"any"`
	NotAll []*ClauseQuery `json:"not_all,omitempty" bson:"not_all"`
	NotAny []*ClauseQuery `json:"not_any,omitempty" bson:"not_any"`
}

type ModelItem struct {
	ID    string `json:"id,omitempty" bson:"id"`
	Title string `json:"title,omitempty" bson:"title"`
}
