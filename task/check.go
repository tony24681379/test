package task

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
	"time"

	cron "gopkg.in/robfig/cron.v2"
)

// Combine schedule rule to string "* * * * *"
func CombineCronRule(scheduleRule *ScheduleRule) string {
	combineArray := func(rules ...string) []string {
		return rules
	}
	rules := combineArray(scheduleRule.Minute, scheduleRule.Hour, scheduleRule.MonthDay, scheduleRule.Month, scheduleRule.WeekDay)
	for _, rule := range rules {
		rule = strings.TrimSpace(rule)
	}
	return strings.Join(rules, " ")
}

// CheckVaildTask check date and task
func CheckVaildTask(now time.Time, taskJob *Task) error {
	startAt, err := checkVaildDate(taskJob.ScheduleRule.ScheduleStartAt)
	if err != nil {
		return err
	}
	endBefore, err := checkVaildDate(taskJob.ScheduleRule.ScheduleEndBefore)
	if err != nil {
		return err
	}

	if now.Before(startAt) || now.After(endBefore) {
		errMsg := "Please make sure this job in a vaild execution time"
		return errors.New(errMsg)
	}

	cronRule := CombineCronRule(taskJob.ScheduleRule)
	if err := checkVaildRule(cronRule); err != nil {
		return err
	}

	if taskJob.Condition == nil {
		return errors.New("condition can't be empty")
	}
	if taskJob.Condition.Meta == nil && taskJob.Condition.Model == nil {
		return errors.New("model and meta can't be empty in the same time")
	}
	return nil
}

// checkVaildDate check date match yyyy/mm/dd
func checkVaildDate(s string) (time.Time, error) {
	// yyyy/mm/dd match 2017/01/01 and 2017/1/1
	re := regexp.MustCompile(`(\d{4})/([12][0-9]|0?[1-9])/([12][0-9]|3[01]|0?[1-9])`)
	result := re.FindStringSubmatch(s)
	if len(result) < 1 {
		return time.Date(0, time.Month(0), 0, 0, 0, 0, 0, time.Local), errors.New(s + " is not a vaild date")
	}
	year, _ := strconv.Atoi(result[1])
	month, _ := strconv.Atoi(result[2])
	day, _ := strconv.Atoi(result[3])
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local), nil
}

// checkVaildRule check the cron rule
func checkVaildRule(s string) error {
	if _, err := cron.Parse(s); err != nil {
		return err
	}
	return nil
}
