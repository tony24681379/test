package task

import (
	"errors"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCheckVaildTask(t *testing.T) {
	tests := []struct {
		now           time.Time
		taskJob       *Task
		expectedError error
	}{
		{
			now: time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			taskJob: &Task{
				ScheduleRule: &ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &Condition{
					Meta: &Meta{},
				},
			},
			expectedError: nil,
		},
		{
			now: time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			taskJob: &Task{
				ScheduleRule: &ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
			},
			expectedError: fmt.Errorf("condition can't be empty"),
		},
		{
			now: time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			taskJob: &Task{
				ScheduleRule: &ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &Condition{},
			},
			expectedError: fmt.Errorf("model and meta can't be empty in the same time"),
		},
		{
			now: time.Date(2016, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			taskJob: &Task{
				ScheduleRule: &ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &Condition{},
			},
			expectedError: fmt.Errorf("Please make sure this job in a vaild execution time"),
		},
		{
			now: time.Date(2018, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			taskJob: &Task{
				ScheduleRule: &ScheduleRule{
					Minute:            "*",
					Hour:              "*",
					MonthDay:          "*",
					Month:             "*",
					WeekDay:           "*",
					ScheduleStartAt:   "2017/01/01",
					ScheduleEndBefore: "2017/12/31",
				},
				Condition: &Condition{},
			},
			expectedError: fmt.Errorf("Please make sure this job in a vaild execution time"),
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		result := CheckVaildTask(test.now, test.taskJob)
		assert.Equal(test.expectedError, result, "Error in num: "+strconv.Itoa(i))
	}
}

func TestCheckVaildDate(t *testing.T) {
	tests := []struct {
		s             string
		expectTime    time.Time
		expectedError error
	}{
		{
			s:             "2017/1/1",
			expectTime:    time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			expectedError: nil,
		},
		{
			s:             "2017/01/01",
			expectTime:    time.Date(2017, time.Month(1), 1, 0, 0, 0, 0, time.Local),
			expectedError: nil,
		},
		{
			s:             "2017/12/31",
			expectTime:    time.Date(2017, time.Month(12), 31, 0, 0, 0, 0, time.Local),
			expectedError: nil,
		},
		{
			s:             "2017/09/1",
			expectTime:    time.Date(2017, time.Month(9), 1, 0, 0, 0, 0, time.Local),
			expectedError: nil,
		},
		{
			s:             "2017/2/01",
			expectTime:    time.Date(2017, time.Month(2), 1, 0, 0, 0, 0, time.Local),
			expectedError: nil,
		},
		{
			s:             "2017/09/",
			expectTime:    time.Date(0, time.Month(0), 0, 0, 0, 0, 0, time.Local),
			expectedError: errors.New("2017/09/ is not a vaild date"),
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		resultTime, err := checkVaildDate(test.s)
		assert.Equal(test.expectTime, resultTime, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedError, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestCheckVaildRule(t *testing.T) {
	tests := []struct {
		scheduleRule  *ScheduleRule
		expectedError error
	}{
		{
			scheduleRule: &ScheduleRule{
				Minute:   "*",
				Hour:     "*",
				MonthDay: "*",
				Month:    "*",
				WeekDay:  "*",
			},
			expectedError: nil,
		},
		{
			scheduleRule: &ScheduleRule{
				Minute:   "1",
				Hour:     "3,6",
				MonthDay: "*",
				Month:    "*",
				WeekDay:  "*",
			},
			expectedError: nil,
		},
		{
			scheduleRule: &ScheduleRule{
				Minute:   "*",
				Hour:     "*",
				MonthDay: "*",
				Month:    "",
				WeekDay:  "",
			},
			expectedError: errors.New("Expected 5 or 6 fields, found 3: * * *  "),
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		cronRule := CombineCronRule(test.scheduleRule)
		err := checkVaildRule(cronRule)
		assert.Equal(test.expectedError, err, "Error in num: "+strconv.Itoa(i))
	}
}
