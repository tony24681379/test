package config

type Datasource struct {
	Name    string   `json:"name,omitempty"`
	Columns []Column `json:"columns,omitempty"`
}

type Column struct {
	Name        string        `json:"name,omitempty"`
	DisplayName string        `json:"displayName,omitempty"`
	Type        string        `json:"type,omitempty"`
	CompareType []CompareType `json:"compareType,omitempty"`
	Options     []CompareType `json:"options,omitempty"`
}

type CompareType struct {
	Code string `json:"code,omitempty"`
	Name string `json:"name,omitempty"`
}
