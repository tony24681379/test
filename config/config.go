package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"os"

	"github.com/golang/glog"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type Configs struct {
	Datasources []*Datasource
	ESAddr      string
	MongoAddr   string
	INUAddr     string
	Port        string
}

func initVariable() {
	flag.Set("alsologtostderr", "true")
	flag.Set("v", "2")
	flag.CommandLine.Parse([]string{})
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.String("es_addr", "http://localhost:9200", "elasticserach address")
	pflag.String("mongo_addr", "localhost:27017", "mongodb address")
	pflag.String("inu_addr", "http://localhost:2403", "inu address")
	pflag.String("datasource", "datasource.json", "amiast datasource config")
	pflag.String("port", "9487", "serve port")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
}

func NewConfig() *Configs {
	initVariable()
	datasourceFileName := viper.GetString("datasource")
	return &Configs{
		Datasources: getDatasourceFromFile(datasourceFileName),
		ESAddr:      viper.GetString("es_addr"),
		MongoAddr:   viper.GetString("mongo_addr"),
		INUAddr:     viper.GetString("inu_addr"),
		Port:        ":" + viper.GetString("port"),
	}
}

func getDatasourceFromFile(datasourceFileName string) []*Datasource {
	var datasources []*Datasource
	if _, err := os.Stat(datasourceFileName); os.IsNotExist(err) {
		return datasources
	}
	datasourceFile, err := ioutil.ReadFile(datasourceFileName)
	if err != nil {
		glog.Fatal(err)
	}
	err = json.Unmarshal(datasourceFile, &datasources)
	if err != nil {
		glog.Fatal(err)
	}
	return datasources
}

func (c *Configs) GetDatasource(name string) *Datasource {
	for _, datasource := range c.Datasources {
		if datasource.Name == name {
			return datasource
		}
	}
	return nil
}
