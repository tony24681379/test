package mongo

import (
	"github.com/golang/glog"
	mgo "gopkg.in/mgo.v2"
)

func NewMongoSession(addr string) *mgo.Session {
	session, err := mgo.Dial(addr)
	if err != nil {
		glog.Fatal("Can not connet to mongodb ", err)
	}
	session.SetMode(mgo.Monotonic, true)
	return session
}
