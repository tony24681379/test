package mongo

import (
	"errors"

	"gitlab.com/grandsys/inu-qms-schedule/task"
)

type fakeTask struct {
	tasks []*task.Task
}

func NewFakeTask(tasks []*task.Task) *fakeTask {
	return &fakeTask{
		tasks: tasks,
	}
}

func (f *fakeTask) AddOrEditTask(task *task.Task) error {
	for i, t := range f.tasks {
		if t.ScheduleRule.RuleCode == task.ScheduleRule.RuleCode {
			f.tasks[i] = task
			return nil
		}
	}
	f.tasks = append(f.tasks, task)
	return nil
}

func (f *fakeTask) GetAllTasks() ([]*task.Task, error) {
	return f.tasks, nil
}

func (f *fakeTask) DeleteTask(ruleCode string) error {
	for i, t := range f.tasks {
		if t.ScheduleRule.RuleCode == ruleCode {
			f.tasks = append(f.tasks[:i], f.tasks[i+1:]...)
			return nil
		}
	}
	return errors.New(ruleCode + " doesn't exist")
}
