package mongo

import (
	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/task"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TaskInterface interface {
	AddOrEditTask(task *task.Task) error
	GetAllTasks() ([]*task.Task, error)
	DeleteTask(ruleCode string) error
}

type MgoTask struct {
	taskCollection *mgo.Collection
}

func NewMgoTask(session *mgo.Session) *MgoTask {
	db := session.DB("qms")

	return &MgoTask{
		taskCollection: db.C("task"),
	}
}

func (t *MgoTask) AddOrEditTask(task *task.Task) error {
	glog.V(4).Info("mongo add task")
	info, err := t.taskCollection.Upsert(bson.M{"scheduleRule.ruleCode": task.ScheduleRule.RuleCode}, task)
	if err != nil {
		return err
	}
	glog.V(4).Infof("upsert id:%s, matched:%d, updated:%d", info.UpsertedId, info.Matched, info.Updated)
	return nil
}

func (t *MgoTask) GetAllTasks() ([]*task.Task, error) {
	glog.V(4).Info("mongo get all task")
	var tasks []*task.Task
	if err := t.taskCollection.Find(nil).All(&tasks); err != nil {
		return nil, err
	}
	glog.V(4).Info(tasks)
	return tasks, nil
}

func (t *MgoTask) DeleteTask(ruleCode string) error {
	glog.V(4).Info("mongo delete task")
	if err := t.taskCollection.Remove(bson.M{"scheduleRule.ruleCode": ruleCode}); err != nil {
		return err
	}
	return nil
}
