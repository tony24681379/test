package controller

import (
	"errors"
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
	"gitlab.com/grandsys/inu-qms-schedule/task"
)

func TestGetTags(t *testing.T) {
	stub := elasticsearch.NewStubElastic()
	elastic := NewElasticController(stub, nil, "")
	result, err := elastic.GetTags()
	expectedResult := []string{"aaa", "bbb"}
	assert.Equal(t, expectedResult, result)
	assert.Equal(t, nil, err)
}

func TestGetModels(t *testing.T) {
	stub := elasticsearch.NewStubElastic()
	elastic := NewElasticController(stub, nil, "")

	tests := []struct {
		tags           string
		from           string
		size           string
		expectedResult []*Model
		expectedErr    error
	}{
		{
			tags: "a",
			from: "0",
			size: "10",
			expectedResult: []*Model{
				{
					ID:    "1",
					Title: "1",
					Tags:  []string{"a", "b"},
				},
				{
					ID:    "2",
					Title: "2",
					Tags:  []string{"b", "c"},
				},
			},
			expectedErr: nil,
		},
		{
			from:           "a",
			size:           "10",
			expectedResult: nil,
			expectedErr: &strconv.NumError{
				Func: "Atoi",
				Num:  "a",
				Err:  errors.New("invalid syntax"),
			},
		},
		{
			from:           "0",
			size:           "b",
			expectedResult: nil,
			expectedErr: &strconv.NumError{
				Func: "Atoi",
				Num:  "b",
				Err:  errors.New("invalid syntax"),
			},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		result, err := elastic.GetModels(test.tags, test.from, test.size)
		assert.Equal(test.expectedResult, result, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGetAllID(t *testing.T) {
	elastic := NewElasticController(nil, nil, "")
	tests := []struct {
		condition  *task.Condition
		expectedID []string
	}{
		{
			condition: &task.Condition{
				Model: nil,
			},
			expectedID: nil,
		},
		{
			condition: &task.Condition{
				Model: &task.Model{
					All: []*task.ModelItem{
						{ID: "1"},
					},
					Any: []*task.ModelItem{
						{ID: "2"},
					},
					NotAll: []*task.ModelItem{
						{ID: "3"},
					},
					NotAny: []*task.ModelItem{
						{ID: "4"},
					},
				},
			},
			expectedID: []string{"1", "2", "3", "4"},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		result := elastic.getAllID(test.condition)
		assert.Equal(test.expectedID, result, "Error in num: "+strconv.Itoa(i))
	}
}

func TestSeperatePercolators(t *testing.T) {
	elastic := NewElasticController(nil, nil, "")
	tests := []struct {
		condition                 *task.Condition
		percolators               []*elasticsearch.Hit
		expectedAllPercolators    []*elasticsearch.ClauseQuery
		expectedAnyPercolators    []*elasticsearch.ClauseQuery
		expectedNotAnyPercolators []*elasticsearch.ClauseQuery
		expectedNotAllPercolators []*elasticsearch.ClauseQuery
		expectedErr               error
	}{
		{
			condition: &task.Condition{
				Model: nil,
			},
			percolators: []*elasticsearch.Hit{
				{ID: "1"},
			},
			expectedAllPercolators:    nil,
			expectedAnyPercolators:    nil,
			expectedNotAnyPercolators: nil,
			expectedNotAllPercolators: nil,
			expectedErr:               fmt.Errorf("Model can't be empty"),
		},
		{
			condition: &task.Condition{
				Model: &task.Model{},
			},
			percolators:               nil,
			expectedAllPercolators:    nil,
			expectedAnyPercolators:    nil,
			expectedNotAnyPercolators: nil,
			expectedNotAllPercolators: nil,
			expectedErr:               fmt.Errorf("Model can't be all empty"),
		},
		{
			condition: &task.Condition{
				Model: &task.Model{
					All: []*task.ModelItem{
						{ID: "1"},
					},
					Any: []*task.ModelItem{
						{ID: "2"},
					},
					NotAny: []*task.ModelItem{
						{ID: "3"},
					},
					NotAll: []*task.ModelItem{
						{ID: "4"},
					},
				},
			},
			percolators: []*elasticsearch.Hit{
				{
					ID: "1",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
				{
					ID: "2",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
				{
					ID: "3",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
				{
					ID: "4",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
			},
			expectedAllPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedAnyPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedNotAnyPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedNotAllPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedErr: nil,
		},
		{
			condition: &task.Condition{
				Model: &task.Model{
					All: []*task.ModelItem{
						{ID: "1"},
					},
					Any: []*task.ModelItem{
						{ID: "2"},
						{ID: "3"},
					},
				},
			},
			percolators: []*elasticsearch.Hit{
				{
					ID: "1",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
				{
					ID: "2",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
				{
					ID: "3",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
			},
			expectedAllPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedAnyPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
				{
					Bool: &elasticsearch.Bool{
						MinimumShouldMatch: 1,
					},
				},
			},
			expectedNotAnyPercolators: nil,
			expectedNotAllPercolators: nil,
			expectedErr:               nil,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		allPercolators, anyPercolators, notAnyPercolators, notAllPercolators, err := elastic.seperatePercolators(test.condition, test.percolators)
		assert.Equal(test.expectedAllPercolators, allPercolators, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedAnyPercolators, anyPercolators, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedNotAnyPercolators, notAnyPercolators, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedNotAllPercolators, notAllPercolators, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGetPercolators(t *testing.T) {
	stub := elasticsearch.NewStubElastic()
	elastic := NewElasticController(stub, nil, "")
	tests := []struct {
		condition      *task.Condition
		expectedResult []*elasticsearch.Hit
		expectedError  error
	}{
		{
			condition: &task.Condition{
				Model: nil,
			},
			expectedResult: nil,
			expectedError:  nil,
		},
		{
			condition: &task.Condition{
				Model: &task.Model{
					All: []*task.ModelItem{
						{ID: "1"},
					},
				},
			},
			expectedResult: []*elasticsearch.Hit{
				{
					ID: "1",
					Source: &elasticsearch.Source{
						Query: &elasticsearch.Query{
							Bool: &elasticsearch.Bool{
								MinimumShouldMatch: 1,
							},
						},
					},
				},
			},
			expectedError: nil,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		result, err := elastic.GetPercolators(test.condition)
		assert.Equal(test.expectedResult, result, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedError, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestSeperateConditionMeta(t *testing.T) {
	elastic := NewElasticController(nil, nil, "")
	tests := []struct {
		condition          *task.Condition
		expectedAllMeta    []*elasticsearch.ClauseQuery
		expectedAnyMeta    []*elasticsearch.ClauseQuery
		expectedNotAnyMeta []*elasticsearch.ClauseQuery
		expectedNotAllMeta []*elasticsearch.ClauseQuery
		expectedErr        error
	}{
		{
			condition:   &task.Condition{},
			expectedErr: fmt.Errorf("Meta can't be empty"),
		},
		{
			condition: &task.Condition{
				Meta: &task.Meta{},
			},
			expectedErr: fmt.Errorf("Meta can't be all empty"),
		},
		{
			condition: &task.Condition{
				Meta: &task.Meta{
					All: []*task.ClauseQuery{
						{
							Term: &elasticsearch.Term{
								Length: 1,
							},
						},
					},
					Any: []*task.ClauseQuery{
						{
							Term: &elasticsearch.Term{
								Length: 2,
							},
						},
					},
					NotAny: []*task.ClauseQuery{
						{
							Term: &elasticsearch.Term{
								Length: 3,
							},
						},
					},
					NotAll: []*task.ClauseQuery{
						{
							Term: &elasticsearch.Term{
								Length: 4,
							},
						},
					},
				},
			},
			expectedAllMeta: []*task.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 1,
					},
				},
			},
			expectedAnyMeta: []*task.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 2,
					},
				},
			},
			expectedNotAnyMeta: []*task.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 3,
					},
				},
			},
			expectedNotAllMeta: []*task.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 4,
					},
				},
			},
			expectedErr: nil,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		allMeta, anyMeta, notAnyMeta, notAllMeta, err := elastic.seperateConditionMeta(test.condition)
		assert.Equal(test.expectedAllMeta, allMeta, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedAnyMeta, anyMeta, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedNotAnyMeta, notAnyMeta, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedNotAllMeta, notAllMeta, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGenerateBoolQuery(t *testing.T) {
	elastic := NewElasticController(nil, nil, "")
	tests := []struct {
		allPercolators    []*elasticsearch.ClauseQuery
		anyPercolators    []*elasticsearch.ClauseQuery
		notAnyPercolators []*elasticsearch.ClauseQuery
		notAllPercolators []*elasticsearch.ClauseQuery
		allMeta           []*elasticsearch.ClauseQuery
		anyMeta           []*elasticsearch.ClauseQuery
		notMeta           []*elasticsearch.ClauseQuery
		notAllMeta        []*elasticsearch.ClauseQuery

		expectedMustBool    []*elasticsearch.ClauseQuery
		expectedShouldBool  []*elasticsearch.ClauseQuery
		expectedMustNotBool []*elasticsearch.ClauseQuery
	}{
		{
			allPercolators:    nil,
			anyPercolators:    nil,
			notAnyPercolators: nil,
			notAllPercolators: nil,
			allMeta:           nil,
			anyMeta:           nil,
			notMeta:           nil,
			notAllMeta:        nil,

			expectedMustBool:    nil,
			expectedShouldBool:  nil,
			expectedMustNotBool: nil,
		},
		{
			allPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Must: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "a",
								},
							},
						},
					},
				},
			},
			anyPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Should: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "c",
								},
							},
						},
					},
				},
			},
			notAnyPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Should: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "e",
								},
							},
						},
					},
				},
			},
			notAllPercolators: nil,
			allMeta: []*elasticsearch.ClauseQuery{
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "b",
					},
				},
			},
			anyMeta: []*elasticsearch.ClauseQuery{
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "d",
					},
				},
			},
			notMeta: []*elasticsearch.ClauseQuery{
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "f",
					},
				},
			},
			notAllMeta: nil,

			expectedMustBool: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Must: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "a",
								},
							},
						},
					},
				},
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "b",
					},
				},
			},
			expectedShouldBool: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Should: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "c",
								},
							},
						},
					},
				},
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "d",
					},
				},
			},
			expectedMustNotBool: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Should: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "e",
								},
							},
						},
					},
				},
				{
					MultiMatch: &elasticsearch.MultiMatch{
						Query: "f",
					},
				},
			},
		},
		{
			allPercolators:    nil,
			anyPercolators:    nil,
			notAnyPercolators: nil,
			notAllPercolators: []*elasticsearch.ClauseQuery{
				{
					Bool: &elasticsearch.Bool{
						Must: []*elasticsearch.ClauseQuery{
							{
								MultiMatch: &elasticsearch.MultiMatch{
									Query: "b",
								},
							},
						},
					},
				},
			},
			allMeta: nil,
			anyMeta: nil,
			notMeta: []*elasticsearch.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 1,
					},
				},
			},
			notAllMeta: []*elasticsearch.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 2,
					},
				},
			},

			expectedMustBool:   nil,
			expectedShouldBool: nil,
			expectedMustNotBool: []*elasticsearch.ClauseQuery{
				{
					Term: &elasticsearch.Term{
						Length: 1,
					},
				},
				{
					Bool: &elasticsearch.Bool{
						Must: []*elasticsearch.ClauseQuery{
							{
								Bool: &elasticsearch.Bool{
									Must: []*elasticsearch.ClauseQuery{
										{
											MultiMatch: &elasticsearch.MultiMatch{
												Query: "b",
											},
										},
									},
								},
							},
							{
								Term: &elasticsearch.Term{
									Length: 2,
								},
							},
						},
					},
				},
			},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		mustBool, shouldBool, mustNotBool := elastic.generateBoolQuery(
			test.allPercolators, test.anyPercolators,
			test.notAnyPercolators, test.notAllPercolators,
			test.allMeta, test.anyMeta,
			test.notMeta, test.notAllMeta,
		)
		assert.Equal(test.expectedMustBool, mustBool, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedShouldBool, shouldBool, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedMustNotBool, mustNotBool, "Error in num: "+strconv.Itoa(i))
	}
}

func TestReturnTaskData(t *testing.T) {
	uuid := &UUIDStub{}
	elastic := NewElasticController(nil, uuid, "http://test")
	tests := []struct {
		task         *task.Task
		result       *elasticsearch.Result
		batchCode    string
		batchStatus  int64
		expectedData *Data
	}{
		{
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					TaskType: "1",
					RuleCode: "2",
				},
			},
			result: &elasticsearch.Result{
				Hits: &elasticsearch.Hits{
					Total: 2,
					Hits: []*elasticsearch.Hit{
						{
							Index: "index",
							Type:  "type",
							ID:    "id1",
							Source: &elasticsearch.Source{
								Query: &elasticsearch.Query{
									Bool: &elasticsearch.Bool{
										MinimumShouldMatch: 1,
									},
								},
							},
						},
						{
							Index: "index",
							Type:  "type",
							ID:    "id2",
						},
					},
				},
			},
			batchCode:   "batchCode",
			batchStatus: 0,
			expectedData: &Data{
				BatchInfo: &BatchInfo{
					BatchCode:   "batchCode",
					BatchStatus: 0,
					TaskType:    "1",
					RuleCode:    "2",
					BatchSum:    2,
				},
				TaskRecord: []*TaskRecord{
					{
						ID:        "id1",
						ReplayURL: "http://test/#!/index/type/id1",
						Source: &elasticsearch.Source{
							Query: &elasticsearch.Query{
								Bool: &elasticsearch.Bool{
									MinimumShouldMatch: 1,
								},
							},
						},
					},
					{
						ID:        "id2",
						ReplayURL: "http://test/#!/index/type/id2",
					},
				},
				UUID: "uuid",
			},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		data := elastic.returnTaskData(test.task, test.result, test.batchCode, test.batchStatus)
		assert.Equal(test.expectedData, data, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGetFirstTask(t *testing.T) {
	elasticStub := elasticsearch.NewStubElastic()
	uuid := &UUIDStub{}
	elastic := NewElasticController(elasticStub, uuid, "http://test")
	tests := []struct {
		task                *task.Task
		size                int64
		expectedData        *Data
		expectedScrollID    string
		expectedBatchCode   string
		expectedBatchStatus int64
		expectedTotal       int64
		expectedErr         error
	}{
		{
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					TaskType: "1",
					RuleCode: "2",
				},
				Condition: &task.Condition{},
			},
			size:                2,
			expectedData:        nil,
			expectedScrollID:    "",
			expectedBatchCode:   "",
			expectedBatchStatus: 0,
			expectedTotal:       0,
			expectedErr:         fmt.Errorf("Meta can't be empty"),
		},
		{
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					TaskType: "1",
					RuleCode: "2",
				},
				Condition: &task.Condition{},
			},
			size:                2,
			expectedData:        nil,
			expectedScrollID:    "",
			expectedBatchCode:   "",
			expectedBatchStatus: 0,
			expectedTotal:       0,
			expectedErr:         fmt.Errorf("Meta can't be empty"),
		},
		{
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					TaskType: "1",
					RuleCode: "2",
				},
				Condition: &task.Condition{
					Model: &task.Model{
						All: []*task.ModelItem{
							{ID: "id1"},
							{ID: "id2"},
						},
					},
				},
			},
			size: 2,
			expectedData: &Data{
				BatchInfo: &BatchInfo{
					BatchCode:   "uuid",
					BatchStatus: 1,
					TaskType:    "1",
					RuleCode:    "2",
					BatchSum:    2,
				},
				TaskRecord: []*TaskRecord{
					{
						ID:        "id1",
						ReplayURL: "http://test/#!/index/type/id1",
						Source: &elasticsearch.Source{
							Title: "1",
						},
					},
					{
						ID:        "id2",
						ReplayURL: "http://test/#!/index/type/id2",
						Source: &elasticsearch.Source{
							Title: "2",
						},
					},
				},
				UUID: "uuid",
			},
			expectedScrollID:    "ScrollID",
			expectedBatchCode:   "uuid",
			expectedBatchStatus: 1,
			expectedTotal:       2,
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		data, scrollID, batchCode, batchStatus, total, err := elastic.GetFirstTask(test.task, test.size)
		assert.Equal(test.expectedData, data, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedScrollID, scrollID, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedBatchCode, batchCode, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedBatchStatus, batchStatus, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedTotal, total, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGetScroll(t *testing.T) {
	elasticStub := elasticsearch.NewStubElastic()
	uuid := &UUIDStub{}
	elastic := NewElasticController(elasticStub, uuid, "http://test")
	tests := []struct {
		scrollID       string
		expectedResult *elasticsearch.Result
		expectedErr    error
	}{
		{
			scrollID: "1",
			expectedResult: &elasticsearch.Result{
				Hits: &elasticsearch.Hits{
					Total: 1,
					Hits: []*elasticsearch.Hit{
						{
							Index: "index",
							Type:  "type",
							ID:    "id1",
							Source: &elasticsearch.Source{
								Title: "1",
							},
						},
					},
				},
			},
		},
		{
			scrollID: "2",
			expectedResult: &elasticsearch.Result{
				Hits: &elasticsearch.Hits{
					Total: 2,
					Hits: []*elasticsearch.Hit{
						{
							Index: "index",
							Type:  "type",
							ID:    "id1",
							Source: &elasticsearch.Source{
								Title: "1",
							},
						},
						{
							Index: "index",
							Type:  "type",
							ID:    "id2",
							Source: &elasticsearch.Source{
								Title: "2",
							},
						},
					},
				},
			},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		result, err := elastic.GetScroll(test.scrollID)
		assert.Equal(test.expectedResult, result, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}

func TestGetScrollTask(t *testing.T) {
	elasticStub := elasticsearch.NewStubElastic()
	uuid := &UUIDStub{}
	elastic := NewElasticController(elasticStub, uuid, "http://test")
	tests := []struct {
		task         *task.Task
		scrollID     string
		batchCode    string
		batchStatus  int64
		expectedData *Data
		expectedErr  error
	}{
		{
			task: &task.Task{
				ScheduleRule: &task.ScheduleRule{
					TaskType: "1",
					RuleCode: "2",
				},
				Condition: &task.Condition{
					Model: &task.Model{
						All: []*task.ModelItem{
							{ID: "id1"},
							{ID: "id2"},
						},
					},
				},
			},
			scrollID:    "2",
			batchCode:   "uuid",
			batchStatus: 1,
			expectedData: &Data{
				BatchInfo: &BatchInfo{
					BatchCode:   "uuid",
					BatchStatus: 1,
					TaskType:    "1",
					RuleCode:    "2",
					BatchSum:    2,
				},
				TaskRecord: []*TaskRecord{
					{
						ID:        "id1",
						ReplayURL: "http://test/#!/index/type/id1",
						Source: &elasticsearch.Source{
							Title: "1",
						},
					},
					{
						ID:        "id2",
						ReplayURL: "http://test/#!/index/type/id2",
						Source: &elasticsearch.Source{
							Title: "2",
						},
					},
				},
				UUID: "uuid",
			},
		},
	}
	assert := assert.New(t)
	for i, test := range tests {
		data, err := elastic.GetScrollTask(test.task, test.scrollID, test.batchCode, test.batchStatus)
		assert.Equal(test.expectedData, data, "Error in num: "+strconv.Itoa(i))
		assert.Equal(test.expectedErr, err, "Error in num: "+strconv.Itoa(i))
	}
}
