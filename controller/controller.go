package controller

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/golang/glog"

	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
	"gitlab.com/grandsys/inu-qms-schedule/task"
)

type ControllerInterface interface {
	GetTags() ([]string, error)
	GetModels(tags, from, size string) ([]*Model, error)
	GetPercolators(condition *task.Condition) ([]*elasticsearch.Hit, error)
	GetFirstTask(task *task.Task, size int64) (data *Data, scrollID, batchCode string, batchStatus, total int64, err error)
	GetScrollTask(task *task.Task, scrollID, batchCode string, batchStatus int64) (data *Data, err error)
	GetScroll(scrollID string) (*elasticsearch.Result, error)
	DeleteScroll(scrollID string)
}

type ElasticController struct {
	receiveAddr string
	inuAddr     string
	elastic     elasticsearch.ElasticInterface
	uuid        UUIDInterface
}

type Model struct {
	ID    string   `json:"id"`
	Title string   `json:"title"`
	Tags  []string `json:"tags"`
}
type Data struct {
	BatchInfo  *BatchInfo    `json:"batchinfo"`
	TaskRecord []*TaskRecord `json:"taskrecord"`
	UUID       string        `json:"UUID"`
}

type BatchInfo struct {
	BatchCode   string `json:"batchcode"`
	BatchStatus int64  `json:"batchstatus"`
	TaskType    string `json:"tasktype"`
	RuleCode    string `json:"rulecode"`
	BatchSum    int64  `json:"batchsum"`
}

type TaskRecord struct {
	ID        string
	ReplayURL string
	*elasticsearch.Source
}

func NewElasticController(elastic elasticsearch.ElasticInterface, uuid UUIDInterface, inuAddr string) *ElasticController {
	return &ElasticController{
		elastic: elastic,
		uuid:    uuid,
		inuAddr: inuAddr,
	}
}

func (e *ElasticController) GetTags() ([]string, error) {
	result, err := e.elastic.GetTags()
	if err != nil {
		return nil, err
	}
	tags := []string{}
	for _, bucket := range result.Aggregations.AllTags.Buckets {
		tags = append(tags, bucket.Key)
	}
	return tags, nil
}

func (e *ElasticController) GetModels(tags, from, size string) ([]*Model, error) {
	if _, err := strconv.Atoi(from); err != nil {
		return nil, err
	}
	if _, err := strconv.Atoi(size); err != nil {
		return nil, err
	}
	// "1,2,3" -> {"1","2","3"}
	tagArray := strings.Split(tags, ",")
	// {"1","2","3"} -> "1 2 3"
	tags = strings.Join(tagArray, " ")

	result, err := e.elastic.GetModels(tags, from, size)
	if err != nil {
		return nil, err
	}
	models := []*Model{}
	for _, hit := range result.Hits.Hits {
		models = append(models, &Model{
			ID:    hit.ID,
			Title: hit.Source.Title,
			Tags:  hit.Source.Tags,
		})
	}
	return models, nil
}

func (e *ElasticController) getAllID(condition *task.Condition) []string {
	ids := []string{}
	if condition.Model == nil {
		return nil
	}
	for _, model := range condition.Model.All {
		ids = append(ids, model.ID)
	}
	for _, model := range condition.Model.Any {
		ids = append(ids, model.ID)
	}
	for _, model := range condition.Model.NotAll {
		ids = append(ids, model.ID)
	}
	for _, model := range condition.Model.NotAny {
		ids = append(ids, model.ID)
	}
	return ids
}

func (e *ElasticController) GetPercolators(condition *task.Condition) ([]*elasticsearch.Hit, error) {
	ids := e.getAllID(condition)
	if ids == nil {
		return nil, nil
	}
	result, err := e.elastic.GetPercolators(ids)
	if err != nil {
		return nil, err
	}

	return result.Hits.Hits, nil
}

func (e *ElasticController) seperatePercolators(condition *task.Condition, percolators []*elasticsearch.Hit) ([]*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, error) {
	if condition.Model == nil {
		return nil, nil, nil, nil, fmt.Errorf("Model can't be empty")
	}
	if condition.Model.All == nil && condition.Model.Any == nil && condition.Model.NotAny == nil && condition.Model.NotAll == nil {
		return nil, nil, nil, nil, fmt.Errorf("Model can't be all empty")
	}
	var allPercolators, anyPercolators, notAnyPercolators, notAllPercolators []*elasticsearch.ClauseQuery

	for _, percolator := range percolators {
		for _, model := range condition.Model.All {
			if model.ID == percolator.ID {
				allPercolators = append(allPercolators, &elasticsearch.ClauseQuery{Bool: percolator.Source.Query.Bool})
			}
		}

		for _, model := range condition.Model.Any {
			if model.ID == percolator.ID {
				anyPercolators = append(anyPercolators, &elasticsearch.ClauseQuery{Bool: percolator.Source.Query.Bool})
			}
		}

		for _, model := range condition.Model.NotAny {
			if model.ID == percolator.ID {
				notAnyPercolators = append(notAnyPercolators, &elasticsearch.ClauseQuery{Bool: percolator.Source.Query.Bool})
			}
		}

		for _, model := range condition.Model.NotAll {
			if model.ID == percolator.ID {
				notAllPercolators = append(notAllPercolators, &elasticsearch.ClauseQuery{Bool: percolator.Source.Query.Bool})
			}
		}
	}

	return allPercolators, anyPercolators, notAnyPercolators, notAllPercolators, nil
}

func (e *ElasticController) seperateConditionMeta(condition *task.Condition) ([]*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, []*elasticsearch.ClauseQuery, error) {
	if condition.Meta == nil {
		return nil, nil, nil, nil, fmt.Errorf("Meta can't be empty")
	}
	if condition.Meta.All == nil && condition.Meta.Any == nil && condition.Meta.NotAny == nil && condition.Meta.NotAll == nil {
		return nil, nil, nil, nil, fmt.Errorf("Meta can't be all empty")
	}
	return condition.Meta.All, condition.Meta.Any, condition.Meta.NotAny, condition.Meta.NotAll, nil
}

func (e *ElasticController) checkConditionModel(condition *task.Condition) error {
	if condition.Model == nil {
		return fmt.Errorf("Model can't be empty")
	}
	if condition.Model.All == nil && condition.Model.Any == nil && condition.Model.NotAny == nil && condition.Model.NotAll == nil {
		return fmt.Errorf("Model can't be all empty")
	}
	return nil
}

func (e *ElasticController) generateBoolQuery(allPercolators, anyPercolators, notAnyPercolators, notAllPercolators, allMeta, anyMeta, notAnyMeta, notAllMeta []*elasticsearch.ClauseQuery) (mustBool, shouldBool, mustNotBool []*elasticsearch.ClauseQuery) {
	notAllBool := append(notAllPercolators, notAllMeta...)

	mustBool = append(allPercolators, allMeta...)
	shouldBool = append(anyPercolators, anyMeta...)
	mustNotBool = append(notAnyPercolators, notAnyMeta...)
	mustNotBool = append(mustNotBool,
		func() []*elasticsearch.ClauseQuery {
			if len(notAllBool) != 0 {
				return []*elasticsearch.ClauseQuery{
					{
						Bool: &elasticsearch.Bool{
							Must: notAllBool,
						},
					},
				}
			}
			return notAllBool
		}()...)
	return mustBool, shouldBool, mustNotBool
}

func (e *ElasticController) GetFirstTask(task *task.Task, size int64) (data *Data, scrollID, batchCode string, batchStatus, total int64, err error) {
	allMeta, anyMeta, notMeta, notAllMeta, metaErr := e.seperateConditionMeta(task.Condition)
	if metaErr != nil && task.Condition.Model == nil {
		return nil, "", "", 0, 0, metaErr
	}
	modelErr := e.checkConditionModel(task.Condition)
	if modelErr != nil && task.Condition.Meta == nil {
		return nil, "", "", 0, 0, modelErr
	}
	if metaErr != nil && modelErr != nil {
		return nil, "", "", 0, 0, fmt.Errorf("%s, %s", metaErr, modelErr)
	}

	percolators, err := e.GetPercolators(task.Condition)
	if err != nil {
		return nil, "", "", 0, 0, err
	}

	allPercolators, anyPercolators, notAnyPercolators, notAllPercolators, err := e.seperatePercolators(task.Condition, percolators)
	if err != nil && task.Condition.Meta == nil {
		return nil, "", "", 0, 0, err
	}

	mustBool, shouldBool, mustNotBool := e.generateBoolQuery(allPercolators, anyPercolators, notAnyPercolators, notAllPercolators, allMeta, anyMeta, notMeta, notAllMeta)
	result, err := e.elastic.GetTaskResult(mustBool, shouldBool, mustNotBool, size)
	if err != nil {
		return nil, "", "", 0, 0, err
	}

	scrollID = result.ScrollID
	batchCode = e.uuid.UUID()
	batchStatus = 0
	if size >= result.Hits.Total {
		batchStatus = 1
	}
	total = result.Hits.Total
	data = e.returnTaskData(task, result, batchCode, batchStatus)

	return
}

func (e *ElasticController) GetScrollTask(task *task.Task, scrollID, batchCode string, batchStatus int64) (data *Data, err error) {
	result, err := e.GetScroll(scrollID)
	if err != nil {
		return nil, err
	}
	data = e.returnTaskData(task, result, batchCode, batchStatus)
	return
}

func (e *ElasticController) returnTaskData(task *task.Task, result *elasticsearch.Result, batchCode string, batchStatus int64) (data *Data) {
	var taskRecord []*TaskRecord

	for _, hit := range result.Hits.Hits {
		taskRecord = append(taskRecord, &TaskRecord{
			ID:        hit.ID,
			ReplayURL: e.inuAddr + `/#!/` + hit.Index + `/` + hit.Type + `/` + hit.ID,
			Source:    hit.Source,
		})
	}
	data = &Data{
		BatchInfo: &BatchInfo{
			BatchCode:   batchCode,
			BatchStatus: batchStatus,
			TaskType:    task.ScheduleRule.TaskType,
			RuleCode:    task.ScheduleRule.RuleCode,
			BatchSum:    result.Hits.Total,
		},
		TaskRecord: taskRecord,
		UUID:       e.uuid.UUID(),
	}
	return
}

func (e *ElasticController) GetScroll(scrollID string) (*elasticsearch.Result, error) {
	result, err := e.elastic.GetScroll(scrollID)
	if err != nil {
		glog.Error(err)
		return nil, err
	}
	return result, nil
}

func (e *ElasticController) DeleteScroll(scrollID string) {
	go func() {
		if err := e.elastic.DeleteScroll(scrollID); err != nil {
			glog.Error(err)
		}
	}()
}
