package controller

type UUIDStub struct {
}

func (s *UUIDStub) UUID() string {
	return "uuid"
}
