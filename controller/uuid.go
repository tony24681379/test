package controller

import "github.com/google/uuid"

type UUIDInterface interface {
	UUID() string
}

type UUID struct {
	uuid uuid.UUID
}

func NewUUID() *UUID {
	return &UUID{}
}

func (u *UUID) UUID() string {
	return uuid.New().String()
}
