package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/job"
	"gitlab.com/grandsys/inu-qms-schedule/task"
)

func postJobTask(elastic controller.ControllerInterface) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("postJobTask")
		var taskJob *task.Task
		err := c.ShouldBindJSON(&taskJob)
		if err == nil {
			j := job.NewJob(taskJob, elastic)
			j.Run()
			c.String(http.StatusOK, "OK")
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}
