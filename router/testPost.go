package router

import (
	"encoding/json"
	"net/http"

	"github.com/golang/glog"

	"github.com/gin-gonic/gin"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
)

func testReceive() gin.HandlerFunc {
	return func(c *gin.Context) {
		var data *controller.Data
		err := c.ShouldBindJSON(&data)
		if err != nil {
			glog.Error(err)
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}
		bytes, err := json.Marshal(data)
		if err != nil {
			glog.Error(err)
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}
		glog.Info(string(bytes))
		c.String(http.StatusOK, "OK")
	}
}
