package router

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
	"gitlab.com/grandsys/inu-qms-schedule/mongo"
	"gitlab.com/grandsys/inu-qms-schedule/schedule"
)

func TestRouterPostTask(t *testing.T) {
	fake := mongo.NewFakeTask(nil)
	scheduler := schedule.NewScheduler(fake, nil)
	router := NewRouter(nil, scheduler, nil)

	bodyString :=
		`
		{
			"scheduleRule":{
				"taskType":"",
				"ruleCode":"1",
				"minute": "*",
				"hour": "*",
				"monthDay":"*",
				"month": "*",
				"weekDay": "*",
				"batchSize": 1000,
				"scheduleStartAt": "2017/1/31",
				"scheduleEndBefore": "2019/12/30",
				"callBackURI": "aaa.bbb.ccc.ddd:port/xxx?",
				"isStart": true
			},
			"condition":{
				"model":{
					"all":[
						{"id" : "1111","title":"?"}
						]
				}
			}
		}
		`
	body := strings.NewReader(bodyString)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/qms/task", body)
	router.ServeHTTP(w, req)
	expectedResult := `{"ok":"ruleCode 1 has been added"}`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/qms/task", nil)
	router.ServeHTTP(w, req)
	expectedResult = `[{"scheduleRule":{"ruleCode":"1","minute":"*","hour":"*","monthDay":"*","month":"*","weekDay":"*","batchSize":1000,"scheduleStartAt":"2017/1/31","scheduleEndBefore":"2019/12/30","isStart":true,"callBackURI":"aaa.bbb.ccc.ddd:port/xxx?"},"condition":{"model":{"all":[{"id":"1111","title":"?"}]}}}]`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/qms/task?ruleCode=1", nil)
	router.ServeHTTP(w, req)
	expectedResult = `{"scheduleRule":{"ruleCode":"1","minute":"*","hour":"*","monthDay":"*","month":"*","weekDay":"*","batchSize":1000,"scheduleStartAt":"2017/1/31","scheduleEndBefore":"2019/12/30","isStart":true,"callBackURI":"aaa.bbb.ccc.ddd:port/xxx?"},"condition":{"model":{"all":[{"id":"1111","title":"?"}]}}}`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("DELETE", "/qms/task?ruleCode=1", nil)
	router.ServeHTTP(w, req)
	expectedResult = `{"ok":"ruleCode 1 has been deleted"}`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/qms/task?ruleCode=1", nil)
	router.ServeHTTP(w, req)
	expectedResult = `{"error":"ruleCode doesn't exist"}`
	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())
}

func TestRouterPostPreview(t *testing.T) {
	stubElastic := elasticsearch.NewStubElastic()
	uuid := new(controller.UUIDStub)
	elastic := controller.NewElasticController(stubElastic, uuid, "http://test")
	fakeMongo := mongo.NewFakeTask(nil)
	scheduler := schedule.NewScheduler(fakeMongo, elastic)
	router := NewRouter(nil, scheduler, nil)

	now := time.Now()
	startDate := strconv.Itoa(now.Year()) + "/" + strconv.Itoa(int(now.Month())) + "/" + strconv.Itoa(now.Day())
	endDate := strconv.Itoa(now.Year()+1) + "/" + strconv.Itoa(int(now.Month())) + "/" + strconv.Itoa(now.Day())
	bodyString :=
		`
	{
		"scheduleRule":{
			"taskType":"",
			"ruleCode":"1",
			"minute": "*",
			"hour": "*",
			"monthDay":"*",
			"month": "*",
			"weekDay": "*",
			"batchSize": 2,
			"scheduleStartAt": "` + startDate + `",
			"scheduleEndBefore": "` + endDate + `",
			"callBackURI": "aaa.bbb.ccc.ddd:port/xxx?",
			"isStart": true
		},
		"condition":{
			"model":{
				"all":[
					{"id" : "1111","title":"?"}
					]
			}
		}
	}
	`
	body := strings.NewReader(bodyString)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/qms/preview?size=2", body)
	router.ServeHTTP(w, req)
	expectedResult := `{"batchinfo":{"batchcode":"uuid","batchstatus":1,"tasktype":"","rulecode":"1","batchsum":2},"taskrecord":[{"ID":"id1","ReplayURL":"http://test/#!/index/type/id1","title":"1"},{"ID":"id2","ReplayURL":"http://test/#!/index/type/id2","title":"2"}],"UUID":"uuid"}`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())
}
