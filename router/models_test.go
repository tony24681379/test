package router

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
)

func TestRouterModels(t *testing.T) {
	stub := elasticsearch.NewStubElastic()
	elastic := controller.NewElasticController(stub, nil, "")
	router := NewRouter(nil, nil, elastic)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/models?size=10&from=0", nil)
	router.ServeHTTP(w, req)
	expectedResult := `[{"id":"1","title":"1","tags":["a","b"]},{"id":"2","title":"2","tags":["b","c"]}]`
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())
}
