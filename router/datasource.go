package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/grandsys/inu-qms-schedule/config"
)

func getDataSource(configs *config.Configs) gin.HandlerFunc {
	return func(c *gin.Context) {
		name := c.Query("name")
		if name == "" {
			c.JSON(http.StatusOK, configs.Datasources)
			return
		}
		if datasource := configs.GetDatasource(name); datasource != nil {
			c.JSON(http.StatusOK, datasource)
			return
		}

		c.JSON(http.StatusNotFound, gin.H{"err": "datasource not found"})
	}
}
