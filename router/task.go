package router

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/schedule"
	"gitlab.com/grandsys/inu-qms-schedule/task"
)

func postTask(scheduler *schedule.Scheduler) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("postTask")
		var task *task.Task
		err := c.ShouldBindJSON(&task)
		if err == nil {
			now := time.Now()
			err = scheduler.AddJob(now, task)
			if err == nil {
				c.JSON(http.StatusOK, gin.H{"ok": "ruleCode " + task.ScheduleRule.RuleCode + " has been added"})
				return
			}
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}

func getTasks(scheduler *schedule.Scheduler) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("getTasks")
		ruleCode := c.Query("ruleCode")
		if ruleCode == "" {
			tasks := scheduler.GetAllJobs()
			c.JSON(http.StatusOK, tasks)
		} else {
			task := scheduler.GetJob(ruleCode)
			if task != nil {
				c.JSON(http.StatusOK, task)
			} else {
				c.JSON(http.StatusNotFound, gin.H{"error": "ruleCode doesn't exist"})
			}
		}
	}
}

func deleteTask(scheduler *schedule.Scheduler) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("deleteTask")
		ruleCode := c.Query("ruleCode")
		if ruleCode != "" {
			err := scheduler.RemoveJob(ruleCode)
			if err == nil {
				c.JSON(http.StatusOK, gin.H{"ok": "ruleCode " + ruleCode + " has been deleted"})
				return
			}
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": "ruleCode " + ruleCode + " doesn't exist"})
	}
}

func postPreview(scheduler *schedule.Scheduler) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("getPreview")
		var task *task.Task
		err := c.ShouldBindJSON(&task)
		size := c.DefaultQuery("size", "100")

		sizeInt64, err := strconv.ParseInt(size, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		now := time.Now()
		data, err := scheduler.PreviewJob(now, task, sizeInt64)
		glog.Error(err)
		if err == nil {
			c.JSON(http.StatusOK, data)
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}
