package router

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grandsys/inu-qms-schedule/config"
)

func TestRouterDatasource(t *testing.T) {
	configs := &config.Configs{
		Datasources: []*config.Datasource{
			{
				Name: "amiast",
				Columns: []config.Column{
					{
						Name:        "customerPhoneNo",
						DisplayName: "客戶電話",
						Type:        "string",
						CompareType: []config.CompareType{
							{Code: "term", Name: "等於"},
							{Code: "terms", Name: "包含"},
						},
					},
					{
						Name:        "customerGender",
						DisplayName: "客戶性別",
						Type:        "enum",
						CompareType: []config.CompareType{
							{Code: "term", Name: "等於"}},
						Options: []config.CompareType{
							{Code: "M", Name: "男"},
							{Code: "F", Name: "女"},
						},
					},
				},
			},
		},
	}
	router := NewRouter(configs, nil, nil)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/datasources?name=amiast", nil)
	router.ServeHTTP(w, req)
	expectedResult := `{"name":"amiast","columns":[{"name":"customerPhoneNo","displayName":"客戶電話","type":"string","compareType":[{"code":"term","name":"等於"},{"code":"terms","name":"包含"}]},{"name":"customerGender","displayName":"客戶性別","type":"enum","compareType":[{"code":"term","name":"等於"}],"options":[{"code":"M","name":"男"},{"code":"F","name":"女"}]}]}`
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/datasources", nil)
	router.ServeHTTP(w, req)
	expectedResult = `[{"name":"amiast","columns":[{"name":"customerPhoneNo","displayName":"客戶電話","type":"string","compareType":[{"code":"term","name":"等於"},{"code":"terms","name":"包含"}]},{"name":"customerGender","displayName":"客戶性別","type":"enum","compareType":[{"code":"term","name":"等於"}],"options":[{"code":"M","name":"男"},{"code":"F","name":"女"}]}]}]`
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/datasources?name=nothing", nil)
	router.ServeHTTP(w, req)
	expectedResult = `{"err":"datasource not found"}`
	assert.Equal(t, 404, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())
}
