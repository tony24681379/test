package router

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	ginglog "github.com/szuecs/gin-glog"
	"gitlab.com/grandsys/inu-qms-schedule/config"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/schedule"
)

func headerMiddleware(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Token, token")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "OPTIONS,GET,POST,PUT,DELETE")
	c.Next()
}

// NewRouter create a router
func NewRouter(configs *config.Configs, scheduler *schedule.Scheduler, elastic controller.ControllerInterface) http.Handler {
	r := gin.New()
	r.Use(ginglog.Logger(3 * time.Second))
	r.Use(gin.Recovery())

	r.Use(headerMiddleware)

	r.GET("/health", checkHealth)
	r.GET("/datasources", getDataSource(configs))
	r.GET("/tags", getTags(elastic))
	r.GET("/models", getModels(elastic))
	qms := r.Group("/qms")
	{
		qms.POST("/task", postTask(scheduler))
		qms.GET("/task", getTasks(scheduler))
		qms.DELETE("/task", deleteTask(scheduler))
		qms.POST("/preview", postPreview(scheduler))
	}

	if gin.IsDebugging() {
		r.POST("/test/task", postJobTask(elastic))
		r.POST("/test/receive", testReceive())
	}

	return r
}
