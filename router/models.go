package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
)

func getModels(elastic controller.ControllerInterface) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("getModels")
		tags := c.Query("tags")
		from := c.DefaultQuery("from", "0")
		size := c.DefaultQuery("size", "10")

		models, err := elastic.GetModels(tags, from, size)
		if err == nil {
			c.JSON(http.StatusOK, models)
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}
