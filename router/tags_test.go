package router

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
	"gitlab.com/grandsys/inu-qms-schedule/elasticsearch"
)

func TestRouterTags(t *testing.T) {
	stub := elasticsearch.NewStubElastic()
	elastic := controller.NewElasticController(stub, nil, "")
	router := NewRouter(nil, nil, elastic)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/tags", nil)
	router.ServeHTTP(w, req)
	expectedResult := `["aaa","bbb"]`
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, expectedResult, w.Body.String())
}
