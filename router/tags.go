package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"gitlab.com/grandsys/inu-qms-schedule/controller"
)

func getTags(elastic controller.ControllerInterface) gin.HandlerFunc {
	return func(c *gin.Context) {
		glog.V(4).Info("getTags")
		tags, err := elastic.GetTags()
		if err == nil {
			c.JSON(http.StatusOK, tags)
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}
